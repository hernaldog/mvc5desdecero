﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MVC5DesdeCeroMS.Models;

namespace MVC5DesdeCeroMS
{
    public class DatosBasicos : DropCreateDatabaseAlways<MusicStoreDB>
    {
        protected override void Seed(MusicStoreDB context)
        {
            context.Genres.Add(new Genre { Name = "Rock" });

            context.Artists.Add(new Artist { Name = "Juanito" });

            context.Albums.Add(new Album
            {
                Artist = new Artist { Name = "Pepe" },
                Genre = new Genre { Name = "Rock Alternativo" },
                Price = 7.99m,
                Title = "Chanta3"
            });
            base.Seed(context);
        }
    }
}