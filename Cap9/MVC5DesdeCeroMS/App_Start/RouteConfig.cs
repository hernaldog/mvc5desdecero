﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVC5DesdeCeroMS
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            //routes.MapMvcAttributeRoutes(); // se mueve afuera
            RegistraRouteTradicional(routes);

            //routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "StoreManager", action = "Index", id = UrlParameter.Optional }
            //);
        }

        public static void RegistraRouteTradicional(RouteCollection routes)
        {
            //routes.MapRoute("basico", "{primero}/{segundo}/{tercero}");
            //routes.MapRoute("basico", "{controller}/{action}");
            //routes.MapRoute("basico", "sitio/{controller}/{action}/{id}");
            //routes.MapRoute("basico", "{controller}/{action}/{id}");
            //routes.MapRoute("basico", "{controller}/{action}/{id}", new { id = UrlParameter.Optional });

            //routes.MapRoute("ruta1", "{controller}/{action}/{id}", new { id = UrlParameter.Optional, action = "Index" });

            //routes.MapRoute("ruta2", "{controller}/{action}/{id}", new { id = UrlParameter.Optional });

            //routes.MapRoute("ruta3", "{controller}/{action}/{id}", new { controller = "Artista", action = "Listar", id = UrlParameter.Optional });

            //routes.MapRoute("ruta4", "", new { controller = "Artista", action = "Listar" });

            //routes.MapRoute("ruta5", "{dia}/{mes}/{agno}", new { controller = "Artista", action = "Index" }, new { dia = @"\d{2}", mes = @"\d{2}", agno = @"\d{4}" });

            //routes.MapRoute("ruta6", "{controller}/{action}/{id}");

            //routes.MapRoute(name: "artista", url: "codigo/version/{action}/{id}", defaults: new { controller = "Artista", action = "Index", id = "" });
            //routes.MapRoute(name: "tienda", url: "{controller}/{action}/{id}", defaults: new { controller = "StoreManager", action = "Index", id = "" });


            //routes.MapPageRoute("fijo", "fijo/url", "~/aspx/Test.aspx");
            //routes.MapRoute("tienda", "{controller}/{action}", new { controller = "StoreManager", action = "Index" });

            

            routes.Add(new Route("{resource}.axd/{*pathInfo}", new StopRoutingHandler()));
            routes.Add(new Route("{resource}.txt/{*pathInfo}", new StopRoutingHandler()));

            routes.MapRoute(
                "EjNamespace",
                "{controller}/{action}/{id}",
                new { controller = "StoreManager", action = "Index", id = "" },
                new[] { "AreasDemoWeb.Controllers" }
                );

            routes.MapRoute("rutacatchall", "{*url}", new { controller = "Error", action = "Error"}); //cuando ponen más path de más, caerá acá.


        }

    }
}
