﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventario.Models
{
    public class Disco
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public int AgnoLanzamiento { get; set; }
        public string Estilo { get; set; }
    }
}