﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TestUnitarios;
using TestUnitarios.Controllers;

namespace Moq
{
    public interface IFoo
    {
        Bar Bar { get; set; }
        string Name { get; set; }
        int Value { get; set; }
        bool DoSomething(string value);
        bool DoSomething(int number, string value);
        string DoSomethingStringy(string value);
        bool TryParse(string value, out string outputValue);
        bool Submit(ref Bar bar);
        int GetCount();
        bool Add(int value);
    }

    public class Bar
    {
        public virtual Baz Baz { get; set; }
        public virtual bool Submit() { return false; }
    }

    public class Baz
    {
        public virtual string Name { get; set; }
    }

    [TestClass]
    public class TestMock
    {
        [TestMethod]
        public void SendMeSomewhereElseIssuesRedirect()
        {
            //var mockContext = new Mock<ControllerContext>();
            //mockContext.Setup(c =>
            //c.HttpContext.Response.Redirect("~/Some/Other/Place"));
            //var controller = new HomeController();
            //controller.ControllerContext = mockContext.Object;
            //controller.SendMeSomewhereElse();
            //mockContext.Verify();
        }

        [TestMethod]
        public void TestBasicoMock()
        {
            var mock = new Mock<IFoo>();

            // Definimos el comportamiento del método GetCount y su resultado
            mock.Setup(m => m.GetCount()).Returns(1);

            // Creamos una instancia del objeto mockeado y la testeamos
            Assert.AreEqual(1, mock.Object.GetCount());
        }

        [TestMethod]
        public void RouteForEmbeddedResource()
        {
            // Arrange
            var mockContext = new Mock<HttpContextBase>();
            mockContext.Setup(c => c.Request.AppRelativeCurrentExecutionFilePath).Returns("~/handler.axd");
            var routes = new RouteCollection();
            RouteConfig.RegisterRoutes(routes);
            // Act
            RouteData routeData = routes.GetRouteData(mockContext.Object);
            // Assert
            Assert.IsNotNull(routeData);
            Assert.IsInstanceOfType(routeData.RouteHandler,
            typeof(StopRoutingHandler));
        }

        [TestMethod]
        public void RutaHomePage()
        {
            var mockContext = new Mock<HttpContextBase>();
            mockContext.Setup(c => c.Request.AppRelativeCurrentExecutionFilePath)
            .Returns("~/");
            var routes = new RouteCollection();
            RouteConfig.RegisterRoutes(routes);
            RouteData routeData = routes.GetRouteData(mockContext.Object);
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Home", routeData.Values["controller"]);
            Assert.AreEqual("Index", routeData.Values["action"]);
            Assert.AreEqual(UrlParameter.Optional, routeData.Values["id"]);
        }
    }
}
