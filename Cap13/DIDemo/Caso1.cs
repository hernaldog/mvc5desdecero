﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIDemo.Caso1
{
    class Caso1
    {
    }
    
    public class ServicioEmail
    {
        public void EnviarMensajeToAll()
        {
            Console.Write("EnviarMensajeToAll");
        }
    }

    public class SistemaNotificacion
    {
        private ServicioEmail serv;
        public SistemaNotificacion()
        {
            serv = new ServicioEmail(); // Este punto evidencia que la clase SistemaNotificacion está muy acoplada
        }
        public void AlertaATodos()
        {
            serv.EnviarMensajeToAll();
        }
    }

    // Comentado por que otro CasoX.cs es el que corre con F5.

    //class Program
    //{
        //static void Main(string[] args)
        //{
        //    SistemaNotificacion n = new SistemaNotificacion();
        //    n.AlertaATodos();
        //    Console.ReadKey();
        //}
    //}
}
