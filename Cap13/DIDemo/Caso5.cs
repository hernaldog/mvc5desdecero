﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Service Locator estático versión mejorada
/// </summary>
namespace DIDemo.Caso5
{
    public interface IMessagingService
    {
        void EnviarMensajeToAll();
    }

    public interface IVoiceService
    {
        void EnviarMensajeToAll();
    }

    public class ServicioEmail : IMessagingService
    {
        public void EnviarMensajeToAll()
        {
            Console.WriteLine("Correo a todos");
        }
    }

    public class ServicioVoz : IVoiceService
    {
        public void EnviarMensajeToAll()
        {
            Console.WriteLine("Voz a todos");
        }
    }

    public static class ServiceLocator
    {
        private static readonly Dictionary<Type, object> registeredServices = new Dictionary<Type, object>();
        public static T GetService<T>()
        {
            return (T)registeredServices[typeof(T)];
        }
        public static void RegisterService<T>(T service)
        {
            registeredServices[typeof(T)] = service;
        }
        public static void Reset()
        {
            registeredServices.Clear();
        }
        public static Int32 Count
        {
            get { return registeredServices.Count; }
        }
    }


    /// <summary>
    /// Sistema de notificación
    /// </summary>
    public class NotificationSystem
    {
        IMessagingService sMail = null;
        public NotificationSystem()
        {
            sMail = new ServicioEmail();
            ServiceLocator.RegisterService(sMail);
        }
        public void UnEventoHaPasado()
        {
            sMail.EnviarMensajeToAll();
        }

        public void UnEventoSuper()
        {
            IVoiceService sVoz = new ServicioVoz();
            ServiceLocator.RegisterService(sVoz);
            IVoiceService voz = ServiceLocator.GetService<IVoiceService>();
            voz.EnviarMensajeToAll();
        }
    }

    /// <summary>
    /// Sistema de notificación 2 sin constructor que registre
    /// </summary>
    public class NotificationSystem2
    {
        IMessagingService sMail = null;
        public NotificationSystem2()
        {}

        public void Inicializar()
        {
            sMail = new ServicioEmail();
            ServiceLocator.RegisterService(sMail);
        }

        public void UnEventoHaPasado()
        {
            sMail.EnviarMensajeToAll();
        }

        public void Procesa1()
        {
            IMessagingService sMail2 = new ServicioEmail();
            ServiceLocator.RegisterService(sMail2);
        }

        public void Procesa2()
        {
            IMessagingService testService1 = ServiceLocator.GetService<IMessagingService>();
            testService1.EnviarMensajeToAll();
        }
    }

    class Program
    {
        /*
        static void Main(string[] args)
        {
            IMessagingService sMail = new ServicioEmail();
           
            ServiceLocator.RegisterService(sMail);     

            IMessagingService testService1 = ServiceLocator.GetService<IMessagingService>();
            testService1.EnviarMensajeToAll();

            NotificationSystem n = new NotificationSystem();
            n.UnEventoHaPasado();

            Console.ReadLine();
        }
        */
    }
}
