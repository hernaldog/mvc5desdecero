﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC5DesdeCeroMS.Controllers
{
    [RoutePrefix("contacto")]
    [Route("{action=index}/{id?}")]
    public class ContactoController : Controller
    {
        public ActionResult Index()
        {
            // Muestra lista de contactos
            return View();
        }
        public ActionResult Details(int id)
        {
            // Muestra detalles por id
            return View();
        }
        public ActionResult Update(int id)
        {
            // Muestra formulario para actualizar por id
            return View();
        }
        public ActionResult Delete(int id)
        {
            // Elimina un contacto por su id
            return View();
        }
    }

}