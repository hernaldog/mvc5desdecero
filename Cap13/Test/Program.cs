﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DIDemo.Caso5;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Caso 1 Test para ver si Service Locator es un Anti-Patrón
                // Se caerá en tiempo de ejecución, no está la interfaz registrada. 
                // El proveedor no tiene como saberlo si no lee documentación.
                //NotificationSystem2 n = new NotificationSystem2();
                //n.UnEventoHaPasado();

                // Para que funcione se debe llamar a Registrar primero (si sabes que es así)
                //NotificationSystem2 n2 = new NotificationSystem2();
                //n2.Inicializar();
                //n2.UnEventoHaPasado();

                // Caso 2 Test para ver si Service Locator es un Anti-Patrón
                // 2-A Si usamos, no hará nada ya que 
                NotificationSystem2 n3 = new NotificationSystem2();
                n3.Procesa1();

                // funciona de suerte Procesa2() ya que adentro no registra nada y gracias a que Procesa1() lo
                // registra, este método funciona, no producción no hay como saberlo.
                n3.Procesa2();

                // 2-B En pruebas unitarias hay que limpiar al final de cada funcion para que no quede "sucia" la siguiente prueba
                NotificationSystem2 n4 = new NotificationSystem2();
                n4.Inicializar();
                n4.UnEventoHaPasado();                
                ServiceLocator.Reset();

                n4.Inicializar();
                n4.Procesa2();
                ServiceLocator.Reset(); // si no limpias corres el riesgo de que falle la prueba Unitaria
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
