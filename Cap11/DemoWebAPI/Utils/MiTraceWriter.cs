﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http.Tracing;

namespace DemoWebAPI.Utils
{
    public class MiTraceWriter: ITraceWriter
    {
        public void Trace(System.Net.Http.HttpRequestMessage request, string category, TraceLevel level, Action<TraceRecord> traceAction)
        {
            TraceRecord record = new TraceRecord(request, category, level);
            traceAction(record);

            string path = HttpContext.Current.Server.MapPath("~/Log.txt");
            File.AppendAllText(path, record.Timestamp.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss") + " - " + record.Status + " - " + record.Level.ToString() + " - " + record.Message + "\r\n");
        }
    }
}