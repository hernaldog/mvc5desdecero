﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MVC5DesdeCeroMS.Models;

namespace MVC5DesdeCeroMS
{
    public class DatosBasicos : DropCreateDatabaseAlways<MusicStoreDB>
    {
        protected override void Seed(MusicStoreDB context)
        {
            context.Genres.Add(new Genre { Name = "Rock" });

            context.Artists.Add(new Artist { Name = "Juanito" });

            context.Albums.Add(new Album
            {
                Artist = new Artist { Name = "Pepe González", Edad = 33 },
                Genre = new Genre { Name = "Rock Alternativo" },
                Price = 7.99m,
                Title = "Chanta3",
                AlbumArtUrl = "mvc5desdecero.cl",
                ImgCaraturla = "disco.jpg"
            });

            context.Albums.Add(new Album
            {
                Artist = new Artist { Name = "Juan Mendex", Edad = 28 },
                Genre = new Genre { Name = "Rock" },
                Price = 9.99m,
                Title = "Lo mejor del 2018",
                ImgCaraturla = "disco2.jpg"
            });

            context.Albums.Add(new Album
            {
                Artist = new Artist { Name = "Ivan el terrible", Edad = 30 },
                Genre = new Genre { Name = "Rock Alternativo" },
                Price = 6.99m,
                Title = "Infra",
                ImgCaraturla = "disco3.jpg"
            });

            context.Albums.Add(new Album
            {
                Artist = new Artist { Name = "Juan Xixi", Edad = 41 },
                Genre = new Genre { Name = "Pop" },
                Price = 8.99m,
                Title = "Lo mejor del 2019",
                ImgCaraturla = "disco4.jpg"
            });

            base.Seed(context);
        }
    }
}