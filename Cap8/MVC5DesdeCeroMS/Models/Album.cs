﻿using MVC5DesdeCeroMS.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC5DesdeCeroMS.Models
{
    public class Album
    {        
        public int AlbumId { get; set; }

        [Display(Name = "Género")]
        public int GenreId { get; set; }

        [Display(Name = "Artista")]
        public int ArtistId { get; set; }

        [Display(Name = "Título")]
        [Required(ErrorMessage = "El Título del Álbum es requerido.")]
        [MaxWords(6, ErrorMessage = "Hay muuuuchas palabras en {0}")]
        [StringLength(140)]
        public string Title { get; set; }

        [Display(Name = "Precio")]
        [Required(ErrorMessage = "Ingrese un precio.")]
        [Range(5, 20, ErrorMessage = "El valor debe ser entre 5 y 20 USD")]
        public decimal Price { get; set; }

        [Display(Name = "Sitio Web del álbum")]
        public string AlbumArtUrl { get; set; }
       
        public virtual Genre Genre { get; set; }
        public virtual Artist Artist { get; set; }

        [Display(Name = "Carátula")]
        public string ImgCaraturla { get; set; }

        //public virtual List<OrderDetail> OrderDetails { get; set; }
    }
}