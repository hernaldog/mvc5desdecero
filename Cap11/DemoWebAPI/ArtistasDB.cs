﻿using DemoWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DemoWebAPI
{
    public class ArtistasDB: DbContext
    {
        public DbSet<Artistas> Artistas { get; set; }
    }
}