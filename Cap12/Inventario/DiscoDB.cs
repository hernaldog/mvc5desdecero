﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Inventario.Models;

namespace Inventario
{
    public class DiscoDB: DbContext
    {
        public DbSet<Disco> Discos { get; set; }
    }
}