﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDI
{
    class Program
    {
        static void Main(string[] args)
        {
            // Inyección de Dependencia. Inyección del constructor
            DIDemo.Caso6.IMessagingService sMail = new DIDemo.Caso6.ServicioEmail();

            DIDemo.Caso6.NotificationSystem n = new DIDemo.Caso6.NotificationSystem(sMail);
            n.UnEventoHaPasado();
            Console.ReadLine();

            // Inyección de Dependencia. Inyección de propiedades

        }
    }
}
