﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// DI con Inyección de Propiedades
/// </summary>
namespace DIDemo.Caso7
{
    public interface IMessagingService
    {
        void EnviarMensajeToAll();
    }

    public class ServicioEmail : IMessagingService
    {
        public void EnviarMensajeToAll()
        {
            Console.WriteLine("Correo a todos");
        }
    }

    /// <summary>
    /// Sistema de notificación
    /// </summary>
    public class NotificationSystem
    {
        public IMessagingService ServicioMensajes
        {
            get;
            set;
        }

        public void UnEventoHaPasado()
        {
            if (ServicioMensajes == null)
            {
                throw new InvalidOperationException("Error: Falta setear ServicioMensajes antes de llamar a este método.");
            }
            ServicioMensajes.EnviarMensajeToAll();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            NotificationSystem n = new NotificationSystem();
            IMessagingService sMail = new ServicioEmail();
            n.ServicioMensajes = sMail;
            n.UnEventoHaPasado();
            Console.ReadLine();
        }
    }
}
