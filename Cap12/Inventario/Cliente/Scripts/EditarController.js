﻿(function (app) {
    var editarController = function ($scope, discoService) {
        $scope.isEditable = function () {
            return $scope.edit && $scope.edit.disco;
        };
        $scope.cancel = function () {
            $scope.edit.disco = null;
        };
        $scope.save = function () {
            if ($scope.edit.disco.Id) {
                updateDisco();
            } else {
                createDisco();
            }
        };
        var updateDisco = function () {
            discoService.update($scope.edit.disco)
                .then(function () {
                    angular.extend($scope.disco, $scope.edit.disco);
                    $scope.edit.disco = null;
                }, function (error) {
                });                
        };
        var createDisco = function () {
            discoService.create($scope.edit.disco)
                .then(function (disco) {
                    $scope.discos.push(disco.data);
                    $scope.edit.disco = null;
                }, function (error) {
                });
        };
    };
    app.controller("EditarController", editarController);
}(angular.module("DiscosModule")));