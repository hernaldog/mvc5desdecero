﻿using DemoWebAPI.Filtros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Tracing;
using log4net;
using log4net.Config;


namespace DemoWebAPI.Controllers
{
    /// <summary>
    /// Documentación general de la Web API
    /// </summary>
    public class ValuesController : ApiController
    {
        //Instancia de Log4Net
        private static readonly ILog log = LogManager.GetLogger(typeof(ValuesController));

        //private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Api que obtiene todos los discos
        /// </summary>
        /// <returns></returns>
        [Route("api/v1/BuscarDiscos")]
        [HttpGet]
        [Log]        
        public IEnumerable<string> Get()
        {
            //ITraceWriter traceWriter = Configuration.Services.GetTraceWriter();
            //traceWriter.Trace(Request, "Mi Categoria", TraceLevel.Info, "{0}", "Hola man.");

            //EscribeTraza("Controllers", "Hola mi viejo perro!", TraceLevel.Warn);

            return new string[] { "value1", "value2" };
        }

        [Route("api/v1/login")]
        [HttpPost]
        public IHttpActionResult Login([FromBody] AccesoObj acceso)
        {
            return Json(new { salida1 = 123, salida2 = 456, salida3 = acceso.user + "-"+ acceso.password });
        }

        /// <summary>
        /// Api que obtiene un disco por su ID
        /// </summary>
        /// <returns></returns>
        [Route("api/v1/BuscarDiscos/{id}")]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            log.Debug("solo para desarrollo DEBUG modeee");

            try
            {
                
                log.Info("Entra a método");

                if (id == 5)
                    return BadRequest("error papa");
                else
                if (id == 6)
                {
                    log.Fatal("Estoy en error fatal");
                    return InternalServerError();
                }
                else
                if (id == 7)
                    return NotFound();
                else
                    return Json(new { nombre = "naldo", libro = "mvc5" });
            }
            catch (Exception ex)
            {
                log.Error("Error general - " + ex.Message + " - " + ex.StackTrace);
                return InternalServerError();
            }
        }

        [Route("api/v1/GetSuma/{param1}/{param2}")]
        [HttpPost]
        public IHttpActionResult GetSuma(int param1, int param2)
        {
            string res = (param1 + param2).ToString();
            return Json(new { salida = res });
        }
               

        // POST api/values
        [Route("api/v1/Post")]
        [HttpPost]
        public HttpResponseMessage Post([FromBody]string nombre)
        {
            return Request.CreateResponse(HttpStatusCode.OK, "Ok nombre=" + nombre);
        }

        [Route("api/v1/GetEjemplo/{id}")]
        [HttpPost]
        public IHttpActionResult GetEjemplo(int id, [FromBody]string valores)
        {
            return Json(new { salida = valores, idEntrada = id });
        }

        [Route("api/v1/GetUbicacion")]
        [HttpGet]
        public HttpResponseMessage GetUbicacion([FromUri]string datosurl)
        {
            return Request.CreateResponse(HttpStatusCode.OK, "datos=" + datosurl);
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }


        private void EscribeTraza(string category, string log, System.Web.Http.Tracing.TraceLevel level)
        {
            ITraceWriter writer = Configuration.Services.GetTraceWriter();

            if (writer != null)
            {
                writer.Trace(
                    Request, category, level,
                    (traceRecord) =>
                    {
                        traceRecord.Message = String.Format("Log personalizado = {0}", log);
                    });
            }
        }

        public class AccesoObj
        {
            public string user { get; set; }
            public string password { get; set; }
        }
    }
}
