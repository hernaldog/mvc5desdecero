﻿(function () {
    var app = angular.module("DiscosModule", ["ngRoute"]);
    var config = function ($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('');
        $routeProvider
            .when("/lista",
                { templateUrl: "/cliente/views/lista.html" })
            .when("/detalle/:id",
                { templateUrl: "/cliente/views/detalle.html" })
            .otherwise(
                { redirectTo: "/lista" })
    };
    app.config(config);
    app.constant("discoApiUrl", "/api/disco/");
}());