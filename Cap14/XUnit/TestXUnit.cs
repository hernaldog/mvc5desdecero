﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TestUnitarios.Controllers;
using Xunit;

namespace XUnit
{
    public class TestXUnit
    {
        
        [Fact(DisplayName = "Prueba XUnit")]
        public void TestLoginXUnit()
        {
            HomeController pobj = new HomeController();
            //int x = pobj.Login("Ajit", "1234");
            //int y = pobj.Login("", "");
            int z = pobj.Login("Admin", "Admin");
            //Assert.AreEqual(-2, y);
            //Assert.AreEqual(-1, x);
            Assert.Equal(0, z);
        }
    }
}
