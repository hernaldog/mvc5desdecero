﻿using DemoWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace DemoWebAPI.Controllers
{
    public class ArtistasController : ApiController
    {
        private ArtistasDB db = new ArtistasDB();

        [Route("api/v1/GetAllArtistas")]
        [HttpGet]
        public IEnumerable<Artistas> GetAll()
        {
            return db.Artistas;
        }

        [Route("api/v1/GetById/{id}")]
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            Artistas artistas = db.Artistas.Find(id);
            if (artistas == null)
            {
                return NotFound();
            }
            return Ok(artistas);
        }

        [Route("api/v1/PutArtista/{id}")]
        [HttpPut]
        public IHttpActionResult PutArtistas(int id, [FromBody] Artistas artistas)
        {
            if (ModelState.IsValid && id == artistas.Id)
            {
                db.Entry(artistas).State = EntityState.Added;
                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    var error = ex.Message;
                    return NotFound();
                }
                return Ok(artistas);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Route("api/v1/DeleteById/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteArtistaById(int id)
        {
            Artistas artistas = db.Artistas.Find(id);
            if (artistas == null)
            {
                return NotFound();
            }
            db.Artistas.Remove(artistas);
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
            return Json(new { salida = "Eliminado ok"});
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
