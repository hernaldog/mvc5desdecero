﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoLibreria
{
    public static class Prueba
    {
        public static int Suma(int a, int b)
        {
            return a + b;
        }

        public static int Multiplica(int a, int b)
        {
            return a * b;
        }
    }
}
