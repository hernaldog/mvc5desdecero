﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC5DesdeCero.Controllers
{
    /// <summary>
    /// Libro MVC 5 Desde Cero. Capítulo 3 - Vistas.
    /// </summary>
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "holaaaa";
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewData["Datos"] = DateTime.Now;
            ViewBag.Message = "Your contact page.";
            ViewBag.MiVariable = "Contactos de ejemplo";

            //return View("Index");
            //return RedirectToAction("About");
            //return View("~/Views/Home/About.cshtml");
            return View();
        }
    }
}