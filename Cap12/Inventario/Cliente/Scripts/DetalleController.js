﻿(function (app) {
    var detalleController = function ($scope, $routeParams, discoService) {
        var id = $routeParams.id;
        discoService
            .getById(id)
            .then(function (discos) {
                $scope.disco = discos.data;
            }, function (error) {
            });

        $scope.edit = function () {
            $scope.edit.disco = angular.copy($scope.disco);
        };
    };
    app.controller("DetalleController", detalleController);
}(angular.module("DiscosModule")));

