﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC5DesdeCeroMS.Models;

namespace MVC5DesdeCeroMS.Controllers
{
    /// <summary>
    /// Libro MVC 5 Desde Cero. Capítulo 7 - Data Annotations y Validaciones.
    /// </summary>
    public class HomeController : Controller
    {
        MusicStoreDB storeDB = new MusicStoreDB();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Search(string q)
        {
            var albums = storeDB.Albums
              .Include("Genre")
              .Include("Artist")
              .Where(a => a.Title.Contains(q))
              .Take(15);
            return View(albums);
        }

        public ActionResult Test(string nombre, string apellido)
        {
            ViewBag.Nombre = nombre;
            ViewBag.Apellido = apellido;
            return View();
        }


    }
}