﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Service Locartor fuertemente tipado
/// </summary>
namespace DIDemo.Caso3
{     
    public interface IMessagingService
    {
        void EnviarMensajeToAll();
    }

    public class ServicioEmail : IMessagingService
    {
        public void EnviarMensajeToAll()
        {
            Console.WriteLine("Holaa a todos");
        }
    }

    public class ServicioSMC : IMessagingService
    {
        public void EnviarMensajeToAll()
        {
            Console.WriteLine("SMS a todos");
        }
    }

    public static class ServiceLocator
    {
        public static IMessagingService ObjService = null;

        //Service locator retorna un valor fuertememte tipado   
        public static IMessagingService SetLocation()
        {
            if (ObjService == null) //caché
            {
                //Te pedieron cambiar la clase que hace todo el negocio ahora es SMS y no Correo
                //no hay que cambiar todas las instancias new algo...sólo debes cambiar esta línea...esa es la gracia

                //ObjService = new ServicioEmail(); 
                ObjService = new ServicioSMC();
                return ObjService;
            }
            return ObjService;
        }
    }

    /// <summary>
    /// Sistema de notificación
    /// </summary>
    public class NotificationSystem
    {
        private IMessagingService svc;

        public NotificationSystem()
        {
            svc = ServiceLocator.SetLocation();
        }
        public void UnEventoHaPasado()
        {
            svc.EnviarMensajeToAll();
        }
    }

    /// <summary>
    /// Sistema nuevo que usa el servicio de envío de correo
    /// </summary>
    public class NotificationSystemPlus
    {
        private IMessagingService svc;

        public NotificationSystemPlus()
        {
            svc = ServiceLocator.SetLocation();
        }
        public void UnEventoHaPasado()
        {
            svc.EnviarMensajeToAll();
        }
    }


    // Comentado por que otro CasoX.cs es el que corre con F5.

    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        // Probando directo el Service Locator
    //        IMessagingService svc = ServiceLocator.SetLocation();
    //        svc.EnviarMensajeToAll();

    //        // probando el caché del Service Locator (usa caché de EmailService no hay que instanciar de nuevo)
    //        svc = ServiceLocator.SetLocation();
    //        svc.EnviarMensajeToAll();

    //        // Probando el sistema de notificación (usa caché de EmailService no hay que instanciar de nuevo)
    //        NotificationSystem n = new NotificationSystem();
    //        n.UnEventoHaPasado();

    //        // De nuevo notificación (usa caché de EmailService no hay que instanciar nuevo)
    //        NotificationSystem n2 = new NotificationSystem();
    //        n.UnEventoHaPasado();

    //        // Otro sistema de notificación  (usa caché de EmailService no hay que instanciar nuevo)
    //        NotificationSystemPlus n3 = new NotificationSystemPlus();
    //        n.UnEventoHaPasado();

    //        Console.ReadKey();
    //    }
    //}
}