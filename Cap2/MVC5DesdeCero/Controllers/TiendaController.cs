﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC5DesdeCero.Controllers
{
    public class TiendaController : Controller
    {
        public string Index()
        {
            return "Hola, ¡este libro esta filete!";
        }

        public string Explorar(string genero)
        {
            string salida = HttpUtility.HtmlEncode("Género es: " + genero);
            return salida;
        }


        public string Detalle(int id)
        {
            return "Estoy en Tienda.Detalle(" + id.ToString() + ")";
        }

    }
}