﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Versión mejorada de Caso 1. Listo para aplicar un forma de IoC.
namespace DIDemo.Caso2
{     
    public interface IMessagingService
    {
        void EnviarMensajeToAll();
    }

    public class ServicioEmail : IMessagingService
    {
        public void EnviarMensajeToAll()
        {
            Console.Write("EnviarMensajeToAll");
        }
    }

    public class NotificationSystem
    {
        private IMessagingService svc;
        public NotificationSystem()
        {
            svc = new ServicioEmail();
        }
        public void UnEventoHaPasado()
        {
            svc.EnviarMensajeToAll();
        }
    }

    class Program
    {
        // Comentado por que otro CasoX.cs es el que corre con F5.

        //static void Main(string[] args)
        //{
        //    NotificationSystem n = new NotificationSystem();
        //    n.UnEventoHaPasado();
        //    Console.ReadKey();
        //}
    }
}