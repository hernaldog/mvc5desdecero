﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVC5DesdeCeroMS.Models;

namespace MVC5DesdeCeroMS.Controllers
{
    [RoutePrefix("StoreManager")]
    //[Route("{action}")]
    public class StoreManagerController : Controller
    {
        private MusicStoreDB db = new MusicStoreDB();

        /// <summary>
        /// Método principal
        /// </summary>
        /// <returns></returns>
        //[Route("~/")]
        [Route("")]
        [Route("home")]
        [Route("index")]
        public ActionResult Index()
        {
            var albums = db.Albums.Include(a => a.Artist).Include(a => a.Genre).OrderBy(b => b.Title);
            return View(albums.ToList());
        }

        // GET: StoreManager/Details/5
        [Route("album/{id}")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            return View(album);
        }

        public ActionResult ListaDiscos()
        {
            return View();
        }

        public ActionResult ListaPaginada(int page)
        {
            return View();
        }


        [Route("{dia}/{mes}/{agno}")]
        public ActionResult BuscaPorFecha(string dia, string mes, string agno)
        {
            ViewBag.Agno = agno;
            ViewBag.Mes = mes;
            ViewBag.Dia = dia;
            return View();
        }

        // GET: StoreManager/Create
        //[Authorize]
        public ActionResult Create()
        {
            ViewBag.ArtistId = new SelectList(db.Artists, "ArtistId", "Name");
            ViewBag.GenreId = new SelectList(db.Genres, "GenreId", "Name");
            return View();
        }

        // POST: StoreManager/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AlbumId,GenreId,ArtistId,Title,Price,AlbumArtUrl")] Album album)
        {
            if (ModelState.IsValid)
            {
                db.Albums.Add(album);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ArtistId = new SelectList(db.Artists, "ArtistId", "Name", album.ArtistId);
            ViewBag.GenreId = new SelectList(db.Genres, "GenreId", "Name", album.GenreId);
            return View(album);
        }

        // GET: StoreManager/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            ViewBag.ArtistId = new SelectList(db.Artists, "ArtistId", "Name", album.ArtistId);
            ViewBag.GenreId = new SelectList(db.Genres, "GenreId", "Name", album.GenreId);
            return View(album);
        }

        // POST: StoreManager/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AlbumId,GenreId,ArtistId,Title,Price,AlbumArtUrl")] Album album)
        {
            if (ModelState.IsValid)
            {
                db.Entry(album).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ArtistId = new SelectList(db.Artists, "ArtistId", "Name", album.ArtistId);
            ViewBag.GenreId = new SelectList(db.Genres, "GenreId", "Name", album.GenreId);
            return View(album);
        }

        // GET: StoreManager/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            return View(album);
        }

        // POST: StoreManager/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Album album = db.Albums.Find(id);
            db.Albums.Remove(album);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Ofertas()
        {
            var album = GetOfertas();
            return PartialView("_Ofertas", album);
        }

        private Album GetOfertas()
        {
            var album = db.Albums
            .OrderBy(a => a.Price)
            .First();
            album.Price *= 0.8m; //20% descuento
            return album;
        }

        //[Route("Buscar")]
        public ActionResult Buscar()
        {
            return View();
        }

        /// <summary>
        /// Buscar con Ajax
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        //[Route("BuscarArtista")]
        public ActionResult BuscarArtista(string q)
        {
            var artistas = GetArtistas(q);
            //throw new Exception("test");
            return Json(artistas, JsonRequestBehavior.AllowGet);
        }

        private List<Artist> GetArtistas(string searchString)
        {
            return db.Artists
            .Where(a => a.Name.Contains(searchString))
            .ToList();
        }

        //[Route("BuscarArtistaAuto")]
        public ActionResult BuscarArtistaAuto(string term)
        {
            var artists = GetArtistasAuto(term).Select(a => new { value = a.Name });
            return Json(artists, JsonRequestBehavior.AllowGet);
        }

        private List<Artist> GetArtistasAuto(string searchString)
        {
            return db.Artists
            .Where(a => a.Name.Contains(searchString))
            .ToList();
        }

        


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
