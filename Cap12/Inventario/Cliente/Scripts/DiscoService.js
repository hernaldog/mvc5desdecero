﻿(function (app) {
    var discoService = function ($http, discoApiUrl) {
        var getAll = function () {
            return $http.get(discoApiUrl);
        };
        var getById = function (id) {
            return $http.get(discoApiUrl + id);
        };
        var update = function (disco) {
            return $http.put(discoApiUrl + disco.Id, disco);
        };
        var create = function (disco) {
            return $http.post(discoApiUrl, disco);
        };
        var destroy = function (disco) {
            return $http.delete(discoApiUrl + disco.Id);
        };
        return {
            getAll: getAll,
            getById: getById,
            update: update,
            create: create,
            delete: destroy
        };
    };
    app.factory("discoService", discoService);
}(angular.module("DiscosModule")));