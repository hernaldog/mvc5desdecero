﻿(function (app) {
    var listaController = function ($scope, discoService) {
        discoService
            .getAll()
            .then(function (discos) {
                $scope.discos = discos.data;
            }, function (error) {
            });
        $scope.create = function () {
            $scope.edit = {
                disco: {
                    Titulo: "",
                    Estilo: "",
                    AgnoLanzamiento: new Date().getFullYear()
                }
            };
        };
        $scope.delete = function (disco) {
            discoService
                .delete(disco)
                .then(function (disco) {
                    removeDiscoById(disco.data.Id);
                }, function (error) {
                });
        };
        var removeDiscoById = function (id) {
            for (var i = 0; i < $scope.discos.length; i++) {
                if ($scope.discos[i].Id == id) {
                    $scope.discos.splice(i, 1);
                    break;
                }
            }
        };
    };
    app.controller("ListaController", listaController);
}(angular.module("DiscosModule")));