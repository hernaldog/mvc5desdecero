namespace DemoWebAPI.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Models;

    internal sealed class Configuration : DbMigrationsConfiguration<DemoWebAPI.ArtistasDB>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DemoWebAPI.ArtistasDB context)
        {
            context.Artistas.AddOrUpdate(m => m.Id, new Artistas { Id = 1, Nombre = "naldo", Edad = 37 });
            context.Artistas.AddOrUpdate(m => m.Id, new Artistas { Id = 1, Nombre = "checho", Edad = 45 });
            context.Artistas.AddOrUpdate(m => m.Id, new Artistas { Id = 1, Nombre = "juan", Edad = 16});
        }
    }
}
