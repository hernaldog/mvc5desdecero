﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestUnitarios;
using TestUnitarios.Controllers;
using TestUnitarios.Models;


namespace TestUnitarios.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        //[TestMethod]
        //public void Index()
        //{
        //    // Arrange
        //    HomeController controller = new HomeController();

        //    // Act
        //    ViewResult result = controller.Index() as ViewResult;

        //    // Assert 2
        //    Assert.IsNotNull(result);
        //}

        [TestMethod]
        public void RouteTest_ejemplo()
        {
            RouteCollection routes = new RouteCollection();            
            RouteConfig.RegisterRoutes(routes);            

            //...
            Assert.IsNotNull(0);
        }

        [TestMethod]
        public void Test_Message_In_ViewBag()
        {
            var Controller = new HomeController();
            var result = Controller.EjemploViewBag("hola") as ViewResult;
            Assert.IsNotNull(result.ViewBag.Mensaje);
        }



        [TestMethod]
        public void IndexConsultaVistaPorDefecto()
        {
            // Arrange
            var controller = new HomeController();
            // Act
            ViewResult result = controller.Index();
            // Assert
            Assert.IsNotNull(result);
            //Assert.IsNull(result.ViewName);
            Assert.AreEqual(string.Empty, result.ViewName);
        }

        [TestMethod]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        }

        [TestMethod]
        public void ValidaAlgo() //mal nombre del test
        {
            var stringCalculator = new StringCalculator();
            var actual = stringCalculator.Add("0");
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void AgregaNumero_RetornaMismoNumero()
        {
            var stringCalculator = new StringCalculator();
            var actual = stringCalculator.Add("0");
            Assert.AreEqual(0, actual);
        }


        [TestMethod]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
