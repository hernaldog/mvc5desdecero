﻿function GetHora() {
    var fecha = new Date();
    var f = fecha.getDate() + "/" + (fecha.getMonth() + 1) + "/" + fecha.getFullYear();
    var h = fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
    return f + " " + h;
}