﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestUnitarios.Models;

namespace TestUnitarios.Controllers
{
    public class HomeController : Controller
    {
        // Login de prueba para NUnit
        public int Login(string UserId, string Password)
        {
            if (string.IsNullOrEmpty(UserId) || string.IsNullOrEmpty(Password))
            {
                return -2;
            }
            else
            {
                if (UserId == "Admin" && Password == "Admin")
                {
                    return 0;
                }
                return -1;
            }
        }

        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ViewResult Index() //mejora para evitar Cast
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        // mal controlador mucho negocio
        public ViewResult ValidaPersona()
        {
            Persona p = new Persona();
            p.Id = 2;
            p.Nombre = "super man";
            p.Edad = 16;
            PersonaBusiness personaBusiness = new PersonaBusiness();
            personaBusiness.AgregarPersonaLista(p);
            bool menorEdad = personaBusiness.ValidaMenordeEdad(p);
            if (menorEdad)
                personaBusiness.NotificarPadres();
            ViewBag.Menor = menorEdad;
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult EjemploViewBag(string valor)
        {
            ViewBag.Mensaje = valor;
            return View();
        }
    }

    // esta clase mal acá, debe estar en su capa, en su propio proyecto de tipo class library
    public class PersonaBusiness
    {
        public PersonaBusiness() { }

        public string AgregarPersonaLista(Persona p)
        {
            p.Id = 1;
            p.Nombre = "naldo";
            //...
            return String.Empty;
        }

        public bool ValidaMenordeEdad(Persona p)
        {
            if (p.Edad < 18)
                return true;
            else
                return false;
        }

        public void NotificarPadres()
        {
            //...
        }

        
    }
}