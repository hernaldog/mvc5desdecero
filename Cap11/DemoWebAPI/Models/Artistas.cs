﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoWebAPI.Models
{
    public class Artistas
    {        
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Edad { get; set; }
    }
}