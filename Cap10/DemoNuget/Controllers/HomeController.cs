﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DemoLibreria;

namespace DemoNuget.Controllers
{
    /// <summary>
    /// Libro MVC 5 Desde Cero. Capítulo 10 - NuGet.
    /// </summary>
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            int salida = Prueba.Suma(4, 6); //usamos función de librería del package
            ViewBag.Salida = salida;
            //int x = 0;
            //int y = 10;
            //int z = y / x;
            return View();
        }

        public ActionResult About()
        {
            

            //throw new Exception("error test");
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}