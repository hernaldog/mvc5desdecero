﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MVC5DesdeCeroMS.Models
{
    public class Register
    {
        [Remote("CheckUserName", "Account")]
        public string UserName { get; set; }
    }
}