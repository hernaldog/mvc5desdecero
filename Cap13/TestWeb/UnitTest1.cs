﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Web.Servicios;
using Web.Controllers;


namespace TestWeb
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var servicioPersonas = new ServicioPersonasDummy();
            HomeController controller = new HomeController(servicioPersonas);
            string resultado = controller.CreaPersona();
            Assert.IsNotNull(resultado);
        }
    }

    public class ServicioPersonasDummy: IServicioPersona
    {
        public string GetNombre()
        {
            return "sergio";
        }
    }
}
