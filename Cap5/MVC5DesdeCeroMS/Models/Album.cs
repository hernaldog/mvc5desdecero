﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC5DesdeCeroMS.Models
{
    public class Album
    {
        public int AlbumId { get; set; }
        public int GenreId { get; set; }
        public int ArtistId { get; set; }

        [Required(ErrorMessage = "El Título del Álbum es requerido.")]
        [StringLength(140)]
        public string Title { get; set; }

        public decimal Price { get; set; }
        public string AlbumArtUrl { get; set; }
        public virtual Genre Genre { get; set; }
        public virtual Artist Artist { get; set; }
        //public virtual List<OrderDetail> OrderDetails { get; set; }
    }
}