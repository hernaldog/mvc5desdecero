﻿$(function () {
    $(".eliminar").mouseover(function () {
        $("#ayuda").fadeIn("slow");
        $("#ayuda").html("Elimina un disco");
        $("#ayuda").fadeOut("slow");
    });

    var fechaHora = GetHora();
    $("#fecha-hora").html(fechaHora);

    // luego de instalar jQueryUI
    $("#album-list img").mouseover(function () {
        //$(this).effect("bounce");
        $(this).effect("bounce", { time: 4, distance: 30 });
    });

    $("input[data-autocomplete-source]").each(function () {
        var target = $(this);
        target.autocomplete({ source: target.attr("data-autocomplete-source") });
    });

    //$("#artistSearch").submit(function (event) {
    //    event.preventDefault();
    //    var form = $(this);
    //    $("#searchresults").load(form.attr("action"), form.serialize());
    //});

 //   $("#artistSearch").submit(function (event) {
 //       event.preventDefault();
 //       var form = $(this);
 //       $.getJSON(form.attr("action"), form.serialize(), function (data)
	//// falta algo
	//});

    //$("#artistSearch").submit(function (event) {
    //    event.preventDefault();
    //    var form = $(this);
    //    $.getJSON(form.attr("action"), form.serialize(), function (data) {
    //        var html = Mustache.to_html($("#artistTemplate").html(),
    //            { artistas: data });
    //        $("#searchresults").empty().append(html);
    //    });
    //});

    //versión final, la pro
    $("#artistSearch").submit(function (event) {
        event.preventDefault();
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            beforeSend: function () {
                $("#ajax-loader").show();
            },
            complete: function () {
                $("#ajax-loader").hide();
            },
            error: searchFailed,
            success: function (data) {
                var html = Mustache.to_html($("#artistTemplate").html(),
                    { artistas: data });
                $("#searchresults").empty().append(html);
            }
        });
    });
});

function searchFailed() {
    $("#searchresults").html("Lo sentimos, hay problemas con la búsqueda.");
}


//$(function () {
//    $("#album-list img").mouseover(function () {
//        $(this).animate({ height: '+=10', width: '+=10' })
//            .animate({ height: '-=10', width: '-=10' });
//    });
//});