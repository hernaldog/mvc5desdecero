namespace Inventario.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Inventario.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<DiscoDB>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DiscoDB context)
        {
            context.Discos.AddOrUpdate(m => m.Titulo,
            new Disco
            {
                Titulo = "Best Hits 2019", AgnoLanzamiento = 2014, Estilo = "Rock"         
            },
            new Disco
            {
                Titulo = "Super Naldo 2019", AgnoLanzamiento = 2019, Estilo = "Pop"
            },
            new Disco
            {
                Titulo = "Pepiche", AgnoLanzamiento = 2012, Estilo = "Jazz"
            },
            new Disco
            {
                Titulo = "Nicos", AgnoLanzamiento = 2012, Estilo = "Alternativa"
            },
            new Disco
            {
                Titulo = "MVC music", AgnoLanzamiento = 2018, Estilo = "Rock"
            });
        }
    }
}
