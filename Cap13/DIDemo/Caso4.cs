﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Service Locator genérico, simple, sin caché
/// </summary>
namespace DIDemo.Caso4
{
    public interface IMessagingService
    {
        void EnviarMensajeToAll();
    }

    public interface IVoiceService
    {
        void EnviarMensajeToAll();
    }

    public class ServicioEmail : IMessagingService
    {
        public void EnviarMensajeToAll()
        {
            Console.WriteLine("Correo a todos");
        }
    }

    public class ServicioVoz : IVoiceService
    {
        public void EnviarMensajeToAll()
        {
            Console.WriteLine("Voz a todos");
        }
    }

    public class ServicioVozGoogle : IVoiceService
    {
        public void EnviarMensajeToAll()
        {
            Console.WriteLine("Dictado por voz de Google");
        }
    }

    public interface IService
    {
        T GetService<T>();
    }


    public class ServiceLocator : IService
    {
        public Dictionary<object, object> servicecontainer = null;
        public ServiceLocator()
        {
            // Se crean y almacenan los Servicios
            // Si nace uno nuevo se cambia acá
            servicecontainer = new Dictionary<object, object>();
            servicecontainer.Add(typeof(IMessagingService), new ServicioEmail());

            //Si te piden cambiar la clase que resuelve el Servicios de Voz basta cambiar acá
            //servicecontainer.Add(typeof(IVoiceService), new ServicioVoz());
            servicecontainer.Add(typeof(IVoiceService), new ServicioVozGoogle());
        }
        public T GetService<T>()
        {
            try
            {
                return (T)servicecontainer[typeof(T)];
            }
            catch (Exception ex)
            {
                throw new NotImplementedException("No existe el servicio.");
            }
        }
    }


    /// <summary>
    /// Sistema de notificación
    /// </summary>
    public class NotificationSystem
    {
        ServiceLocator loc = new ServiceLocator();
        IMessagingService svc;

        public NotificationSystem()
        {            
            svc = loc.GetService<IMessagingService>();
           
        }
        public void UnEventoHaPasado()
        {
            svc.EnviarMensajeToAll();
        }
    }

    class Program
    {
        /*
        static void Main(string[] args)
        {
            ServiceLocator loc = new ServiceLocator();
            IMessagingService srvM = loc.GetService<IMessagingService>();
            srvM.EnviarMensajeToAll();            

            IVoiceService srvV = loc.GetService<IVoiceService>();
            srvV.EnviarMensajeToAll();            

            NotificationSystem n = new NotificationSystem();
            n.UnEventoHaPasado();

            //Error a propósito ya que aguanta todo la versión Service Locator genérica
            //TestError error = loc.GetService<TestError>();
            //error.EnviarMensajeToAll();

            Console.ReadLine();
        }*/
    }

    class TestError
    {
        public void EnviarMensajeToAll() { }
    }

    
}
