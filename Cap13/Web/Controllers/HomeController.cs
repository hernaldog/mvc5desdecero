﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Servicios;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private IServicioPersona _servicioPersona;

        //Se comenta ya que tiene mucho acoplamiento de Home con la implementación de sus dependencias.
        //public HomeController()
        //{
        //    ServicioPersona serv = new ServicioPersona();
        //    OtroServicio otro = new OtroServicio();
        //    //etc..
        //}

        public HomeController(IServicioPersona servicioPersona)
        {
            _servicioPersona = servicioPersona;
        }

        public ActionResult Index()
        {
            var data = _servicioPersona.GetNombre();
            ViewBag.Datos = data;
            return View();
        }

        public string CreaPersona()
        {
            return "ok";
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}