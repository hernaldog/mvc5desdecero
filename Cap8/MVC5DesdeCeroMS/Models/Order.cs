﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MVC5DesdeCeroMS.Utils;
using System.ComponentModel;

namespace MVC5DesdeCeroMS.Models
{
    public class Order : IValidatableObject
    {
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (FirstName != null && FirstName.IndexOf(".") > 0)
            {
                yield return new ValidationResult("¡No puede llevar punto!", new[] { "FirstName" });
            }
        }

        
        [Display(Name = "Id de Compra")]
        public int OrderId { get; set; }

        [UIHint("TIP: Usa AAAA/MM/DD")]
        [Display(Name = "Fecha de compra")]
        public DateTime OrderDate { get; set; }


        [Display(Order = 15002)]
        [ScaffoldColumn(false)]
        public string Username { get; set; }

        //[Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [StringLength(160), MinLength(5)]
        [Display(Name = "Nombre", Order =15000)]
        public string FirstName { get; set; }

        //[Required]
        //[StringLength(160)]
        //public string LastName { get; set; }

        //[Required(ErrorMessage = "Ingresa tu apellido.")]
        //[StringLength(160, ErrorMessage = "El apellido es muy largo.")]
        //public string LastName { get; set; }

        [Required(ErrorMessage = "Ingresa el {0}.")]
        [StringLength(160, ErrorMessage = "{0} es muy largo.")]
        [MaxWords(5, ErrorMessage = "Hay muuuuchas palabras en {0}")]
        [Display(Name = "Apellido", Order = 15001)]
        public string LastName { get; set; }

        [Display(Name = "Dirección")]
        [DisplayFormat(ConvertEmptyStringToNull = true, NullDisplayText = "[Sin dirección]")]
        public string Address { get; set; }

        [Display(Name = "Ciudad")]
        public string City { get; set; }

        [Display(Name = "Región")] //En Chile no hay Estados, si Regiones
        public string State { get; set; }

        [Display(Name = "Código Postal")]
        public string PostalCode { get; set; }

        [Display(Name = "País")]
        public string Country { get; set; }

        [Display(Name = "Teléfono")]
        public string Phone { get; set; }

        //[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}")]
        //public string Email { get; set; }

        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Ingresa un correo correcto.")]
        [Display(Name = "Correo")]
        public string Email { get; set; }

        [Compare("Email")]
        [Display(Name = "Repita Correo")]
        public string EmailConfirm { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        [ReadOnly(true)]
        public int Total { get; set; }

        //[Range(18, 99)]
        public int Edad { get; set; }

        //[Range(typeof(decimal), "0,00", "19,99")]
        [Display(Name = "Valor")]
        [DataType(DataType.Currency)]
        public int Price { get; set; }

        
              

        //public List<OrderDetail> OrderDetails { get; set; }


    }
}