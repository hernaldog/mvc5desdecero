﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// DI con Inyección del constructor.
/// </summary>
namespace DIDemo.Caso6
{
    public interface IMessagingService
    {
        void EnviarMensajeToAll();
    }

    public class ServicioEmail : IMessagingService
    {
        public void EnviarMensajeToAll()
        {
            Console.WriteLine("Correo a todos");
        }
    }

    /// <summary>
    /// Sistema de notificación
    /// </summary>
    public class NotificationSystem
    {
        private IMessagingService svc;
        public NotificationSystem(IMessagingService servicio)
        {
            this.svc = servicio;
        }
        public void UnEventoHaPasado()
        {
            svc.EnviarMensajeToAll();
        }
    }    

    class Program
    {
        /*
        static void Main(string[] args)
        {
            IMessagingService sMail = new ServicioEmail();
            NotificationSystem n = new NotificationSystem(sMail);
            n.UnEventoHaPasado();
            Console.ReadLine();            
        }
        */
    }
}
